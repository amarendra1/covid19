import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
import VueAxios from "vue-axios";

// import Chart from 'chart.js';
import "./assets/styles/index.css"

import './registerServiceWorker'

Vue.use(VueAxios, axios);
axios.defaults.baseURL = "https://api.covid19api.com";
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
