const fs = require('fs')
module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? "/covid19/" : '/',
    devServer: {
        https: true,
        key: fs.readFileSync('./certs/server.key'),
        cert: fs.readFileSync('./certs/server.cert'),
    },
    pwa: {
        name: 'covid19'
    }
}
